package com.techu.backend.model;

//import org.hibernate.annotations.*;
//import org.jetbrains.annotations.NotNull;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;


import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "PRODUCTOS")
public class ProductoModel {
    @Column(name = "ID")
    @Id
    private String id;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "PRECIO")
    private double precio;

    //private List<UserModel> users;

    // Constructor vacío:
    public ProductoModel() {

    }

    public ProductoModel(String id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    //public List<UserModel> getUsers() {
    //    return users;
    //}
    //
    //public void setUsers(List<UserModel> users) {
    //    this.users = users;
    //}
}

package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoJpaService {

    @Autowired
    ProductoRepository productoRepository;

    // READ
    public List<ProductoModel> findAll() {

        return productoRepository.findAll();
    }

    // CREATE
    public ProductoModel save(ProductoModel newProducto) {

        return productoRepository.save(newProducto);
    }

    // READ por id
    public Optional<ProductoModel> findById(String id) {

        return productoRepository.findById(id);
    }

    //DELETE y en lugar de por id vamos a hacerlo por objeto completo aunque no es necesario
    public boolean delete(ProductoModel productoModel ) {
        try {
            productoRepository.delete(productoModel);
            return true;
        } catch (Exception ex) {
            return false;
        }

    }

}

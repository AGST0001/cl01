package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.techu.backend.service.ProductoJpaService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoJpaService productoService;

    @GetMapping("")
    public String home() {

        return "API REST Tech U Ana v2.0.0";
    }

    // GET a todos los productos del recurso productos y se le llama colección
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        //return "Listado de productos Ana";
        return productoService.findAll();
    }
    // POST a recurso productos para crear un producto
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {
        productoService.save(newProduct);
        return newProduct;
    }

    // GET a un unico producto por id que ademas lo muestra en pantalla
    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoById(@PathVariable String id) {

        return productoService.findById(id);
    }

    // PUT
    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate) {
        productoService.save(productoToUpdate);
    }

    //DELETE por el producto que llegará en el body
    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }

}
